import numpy as np
import tensorflow as tf

from os import path, makedirs
from datetime import datetime
from utils import io, video

class Logger():

    def __init__(self, model, logdir, num_epochs):
        self.model = model
        self.writer_train = tf.summary.create_file_writer(
                            path.join(logdir, "tb/train"))
        self.writer_test = tf.summary.create_file_writer(
                            path.join(logdir, "tb/test"))

        self.videos_path = path.join(logdir, 'videos')
        self.log_path = path.join(logdir, 'log')
        self.sp = int(np.log10(num_epochs)) + 1

        makedirs(logdir, exist_ok = True)

        # Advance full video iterators if training is resumed from checkpoint

        for i in range(model.epoch.numpy() - 1):
            _ = model.data.full_train_ds.next()
            _ = model.data.full_test_ds.next()


    def summarize(self, epoch, log_dict):

        model = self.model

        losses_train = log_dict['losses_train']
        losses_test = log_dict['losses_test']
        dict_train = log_dict['dict_train']
        dict_test = log_dict['dict_test']

        with self.writer_train.as_default():

            for i, k in enumerate(model.n_hist):
                with tf.name_scope(model.f_hist[i]):
                    tf.summary.histogram(k, dict_train[k], step=epoch)

            for i, k in enumerate(model.n_losses):
                with tf.name_scope(model.f_losses[i]):
                    tf.summary.scalar(k, losses_train[i], step=epoch)

            self.writer_train.flush()

        with self.writer_test.as_default():

            for i in model.i_hist:
                with tf.name_scope(model.f_hist[i]):
                    k = model.n_hist[i]
                    tf.summary.histogram(k, dict_test[k], step=epoch)
            for i in model.i_losses:
                with tf.name_scope(model.f_losses[i]):
                    k = model.n_losses[i]
                    tf.summary.scalar(k, losses_test[i], step=epoch)

            self.writer_test.flush()

        def write_vid(mode, img):
            io.write_video_gif(path.join(self.videos_path,
                                            '%07d%s' % (epoch, mode)), img)

        write_vid('t', video.get_vid(model.data.full_train_ds, model))
        write_vid('v', video.get_vid(model.data.full_test_ds, model))

        log_entry = ['Ep. %s' % (('%d' % epoch).ljust(self.sp))]

        for i in range(len(model.n_losses)):
            loss_name = model.n_losses[i]
            log_entry += ['{} {:.3e} {:.3e}'.format(
                loss_name, losses_train[i], losses_test[i])]

        log_entry += ['[%s]' % (
            str(datetime.now() - log_dict['start_time'])[:-4])]

        log_entry = ' '.join(log_entry)

        with open(self.log_path, 'a') as f:
            f.write(log_entry + '\n')

    def close(self):
        self.writer_train.close()
        self.writer_test.close()
