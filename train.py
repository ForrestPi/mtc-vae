import tensorflow as tf

from os import path
from tqdm import tqdm
from logger import Logger
from decimal import Decimal
from datetime import datetime
from models.mtc_vae import MTC_VAE
from utils import config, model_utils
from datasets import TrainingDatasets

class BaseTrainer:

    def __init__(self, model, num_epochs, checkpoint_every, logdir):

        self.model = model
        self.data = model.data
        self.num_epochs = num_epochs
        self.checkpoint_every = checkpoint_every
        self.logdir = logdir

    def train(self):
        self.start_time = datetime.now()
        for cur_epoch in range(self.model.epoch.numpy(),
                                                    self.num_epochs + 1, 1):
            self.train_epoch()
            self.model.epoch.assign(self.model.epoch + 1)
            if cur_epoch % self.checkpoint_every == 0:
                ckpt_path = path.join(self.logdir, 'checkpoints/')
                self.model.ckpt_manager.save()
        self.logger.close()
        print("Total elapsed time: %s" % str(datetime.now() - self.start_time))

    def train_epoch(self):
        raise NotImplementedError

class Trainer(BaseTrainer):

    def __init__(self, model, num_epochs, checkpoint_every, logdir, batch_size,
        num_epoch_repeats, chunk_shape, **kwargs):

        self.num_epochs = num_epochs
        self.batch_size = batch_size
        self.chunk_size = chunk_shape[0]
        self.num_epoch_repeats = num_epoch_repeats

        self.bar_format = ' '.join(['{l_bar}{bar}|',
            '{elapsed}<{remaining}>{postfix}'])

        BaseTrainer.__init__(self, model, num_epochs, checkpoint_every, logdir)
        self.logger = Logger(model, logdir, num_epochs)

    def train_epoch(self):

        model = self.model
        model.train = True

        losses_train = []

        epoch_size = -((self.data.chunk_train_ds.size * self.num_epoch_repeats)\
                            // -self.batch_size)
        
        epoch = model.epoch.numpy()
        progress_bar = tqdm(total=epoch_size, desc='Epoch ' + str(epoch) + '/'
                + str(self.num_epochs), bar_format=self.bar_format)

        for i in range(epoch_size):
            dict_train = model.gen_step()
            losses_train.append(tf.stack([dict_train[v] \
                for v in model.n_losses]))
            progress_bar.update(1)

        losses_train = tf.reduce_mean(tf.stack(losses_train),
            axis=0).numpy().tolist()

        postfix_dict = {model.n_losses[i] : '%.3e' % Decimal(losses_train[i]) \
            for i in range(len(losses_train))}
        progress_bar.set_postfix(postfix_dict)
        progress_bar.close()

        model.train = False

        losses_test = []
        for i in range(-(self.data.chunk_test_ds.size // -self.batch_size)):
            x, _, _, _ = self.data.chunk_test_ds.next()
            dict_test = model.forward(x, x)
            losses_test.append(tf.stack([dict_test[v] for v in model.n_losses]))

        losses_test = tf.reduce_mean(tf.stack(losses_test),
            axis=0).numpy().tolist()

        logger_dict = {'dict_train' : dict_train,
                        'dict_test' : dict_test,
                     'losses_train' : losses_train,
                      'losses_test' : losses_test,
                       'start_time' : self.start_time}

        self.logger.summarize(epoch, logger_dict)


if __name__ == '__main__':

    args = config.get_args()

    data = TrainingDatasets(**args)
    model = MTC_VAE(data, **args)
    trainer = Trainer(model, **args)

    model_utils.model_summary(model, args['logdir'])
    trainer.train()
    
