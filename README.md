# MTC-VAE

This repository contains the Tensorflow 2.3.0 code of the paper **Video Reenactment as Inductive Bias for Content-Motion Disentanglement** by Juan F. Hernández Albarracín and [Prof. Dr. Adín Ramírez Rivera](https://ic.unicamp.br/~adin/).

## Introduction

We propose the Motion-Transfer Chunk Variational Autoencoder (MTC-VAE), a model to disentangle motion and content from video, by representing temporal segments (a.k.a. chunks) as independent random variables, transforming them into a disentangled latent distribution, from which video chunks can be sampled consistently. We provide an extensive compilation of experimental results on video reenactment by MTC-VAE in [this web page](https://mipl.gitlab.io/mtc-vae/).

### Model description

We represent the video $`x = (x_{k})_{k=1}^K`$ as a sequence of $`K`$ non-overlapping and equally-sized chunks~$`x_{k}`$.
Each chunk contains $`c`$ consecutive frames from $`x`$.
Similarly, we define $`w = \left(w_k\right)_{k=1}^K`$ as the sequence of motion representations corresponding to each~$`x_k`$. 
For the $`k`$-th chunk, we model the content and motion as independent latent variables $`z`$ and $`w_k`$, respectively.
We assume $`z`$ to be unique for all the chunks, as content comprises visual features that remain constant through time.

![](md_sources/scheme.png)

## Video Reenactment Examples

<details>
  <summary>Click to view reenactment examples</summary>
  
  ### 3dShapes
![](md_sources/3dShapes.gif)
### CK
![](md_sources/CK.gif)
### dSprites
![](md_sources/dSprites.gif)
### LPC
![](md_sources/LPC.gif)
### MMNIST
![](md_sources/MMNIST.gif)
</details>

## Running the code

### Setup

#### Using pip

```sh
pip install -r requirements.txt
```

#### Using docker

We provide [docker images](https://hub.docker.com/r/mipl/mtc-vae/) built with the latest code in this repository. So you can start an interactive container by running

```sh
docker run --rm -it --userns=host --runtime=nvidia mipl/mtc-vae:latest
```

We recommend to mount the necessary volumes if you want to grab results, or modify the source code.

### Data format

So far, we only provide support on the frame mosaic format, as it can be seen in [this example](data/MMNIST/0/diag_1/3.jpg), since this format has the best I/O performance.
One video in the frame mosaic format is an $`H \times W`$ image, where

```python
# f : number of frames
# h : frame height
# w : frame width
H = h * int(np.sqrt(f))
W = w * int(np.ceil(f / H))
```

Since the exact number of frames is not recoverable from the image mosaic dimensions, it must be saved externally.
We use plain text files to specify the length of one video and its ground-truth factors of variation.
Each line of this file corresponds to a video. Its format is the following:

```
<path to video> <num_frames> <factor 1> <factor 2> ... <factor n>
```

for datasets with $`n`$ factors of variation.
An example of list file can be found [here](data/lists/MMNIST_train.list).
In this example, the MMNIST dataset has only two factors of variation: digit identity and type of motion.
All the videos have 32 frames.

#### Quick suggestion to export your data to the frame mosaic format

The [montage](http://www.imagemagick.org/Usage/montage/) command from ImageMagick automatically creates the mosaic from a list of images, with the $`H \times W`$ image size, as defined above.
So if you have your videos in the standard format of frames in independent images inside a folder, you can create the mosaic with the following command:


```sh
montage <path to frames folder>/* -geometry x+0+0 <output file name>
```

### Demos

This library supports YAML configuration files.
An example of configuration file can be found [here](config/MMNIST.yaml).
We provide additional configuration files in `config/paper` for the five datasets reported in the paper, with the configuration for each dataset to obtain the best results reported.
However, this repository does not provide the whole datasets, so training the model with such files may return an error, unless you prepare the corresponding datasets, as described above and update the paths to the list files.

We provide a running tiny example on the Moving MMNIST dataset, through the `config/MMNIST.yaml` file.
The instructions below on how to use the library are based in this tiny example.

#### Training the model

Run the following command

```sh
python train.py --mgrowth --config config/MMNIST.yaml --logdir log/example_mmnist
```

The `--mgrowth` flag prevents the script to occupy the whole GPU memory, and use it under demand instead.

Please, notice that, as this is a tiny demo, the script is expected to finish in a few minutes and it trains the model with very few data, so no good results are expected from such execution.

#### Saving embeddings

After training the model, the following command

```sh
python embed.py --mgrowth --logdir log/example_mmnist
```

reads the whole train and test dataset, calculates the latent vectors from all the examples, and saves them in Numpy format, along with the factors of variation.

#### Performing reenactment

The following command

```sh
python reenact.py --mgrowth --logdir log/example_mmnist
```

reads a list containing the source/driving pairs and performs reenactment between the corresponding videos.
The list must include the path and the length of the videos, in the following format:

```
<path to source> <num_frames source> <path to driving> <num_frames driving>
```

[Here](data/lists/MMNIST.csv) is an example of a paired list file for reenactment.

### Training the model with your own data

Please, pre-process your data as described above and generate the list files. You can follow the example provided in [the data folder](data).

We additionally provide generation scripts for the [3dShapes](https://gitlab.com/mipl/moving-3dshapes) and [dSprites](https://gitlab.com/mipl/moving-dsprites) datasets, which let you create the datasets in the format required by our model.

