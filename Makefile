train:
	python train.py \
		--dataset     data/lists/MMNIST_train.list \
		--val_dataset data/lists/MMNIST_test.list \
		--prompt_every 10 \
		--output_path log/example \
		--n_epochs 1000 \
		--frame_size 64 \
		--dim_zA 8 \
		--dim_zM 4 \
		--chunk_size 5 \
		--beta 1.0 \
		--order 2 \
		--n_channels 1 \
		--vlambda 1.0 \
		--mgrowth

embed:
	python embed.py \
		--dataset     data/lists/MMNIST_train.list \
		--val_dataset data/lists/MMNIST_test.list \
		--output_path log/example \
		--batch_size 10 \
		--mgrowth \

reenact:
	python reenact.py \
		--dataset data/lists/MMNIST.csv \
		--output_path log/example \
		--batch_size 10 \
		--mgrowth \
