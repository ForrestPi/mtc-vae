from os import path
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.layers import Activation, Conv3D, Lambda, Reshape, Conv3DTranspose
from tensorflow_probability import distributions as tfpd

def Sigmoid():
  return Activation(keras.activations.sigmoid)

def Softplus():
  return Activation(keras.activations.softplus)

def Conv3d(nc, fs, strd, pd, name):
  return Conv3D(nc, fs, strides = strd, padding = pd,
    kernel_initializer = 'TruncatedNormal', bias_initializer = 'Zeros',
    name = name)
def sConv3d(nc, fs, strd, name):
  return Conv3d(nc, fs, strd, 'same', name)
def vConv3d(nc, fs, strd, name):
  return Conv3d(nc, fs, strd, 'valid', name)

def Deconv3d(nc, fs, strd, pd, name):
  return Conv3DTranspose(nc, fs, strides = strd,
    padding = pd, kernel_initializer = 'TruncatedNormal',
    bias_initializer = 'Zeros', name = name)
def sDeconv3d(nc, fs, strd, name):
  return Deconv3d(nc, fs, strd, 'same', name)
def vDeconv3d(nc, fs, strd, name):
  return Deconv3d(nc, fs, strd, 'valid', name)

def Dense(n, name):
  return keras.layers.Dense(n, kernel_initializer = 'TruncatedNormal',
    bias_initializer = 'Zeros', name = name)

def batch2vid(x):
    _, f, h, w, c = x.shape
    n = tf.shape(x)[0]
    return tf.reshape(x, [n * f, h, w, c]), f

def model_summary(model, root):
    tt = 0
    print('\n------------------')
    print('    Model size')
    print('------------------')
    with open(path.join(root, 'model_summary'),'w') as fh:
        for mod in model.modules:
            cp = mod.count_params()
            tt += cp
            print(' %s\t%s' % (mod.name, ('%d' % cp).rjust(8)))
            mod.summary(print_fn = lambda x : fh.write(x + '\n'))
    print('-----------------')
    print(' Total\t%s' % (('%d' % tt).rjust(8)))
    print('-----------------\n')
    

def dyn_fs(cs):

  # Returns the frame filter size for valid convolutions in the decoder
  # according to: 4 + 2(f1 - 1) + 4(f0 - 1) = a

  a = max(4, cs + (cs % 2))

  if a % 6 == 0:
      f0 = a // 6
      f1 = f0 + 1
  elif (a - 2) % 6 == 0:
      f1 = (a - 2) // 6
      f0 = f1 + 1
  elif (a + 2) % 6 == 0:
      f0 = (a + 2) // 6
      f1 = f0

  return f0, f1, 1 + a - cs

# Keras Lambda Layers

def lk(batch, fs):

    def stack_neigh(I):
        return tf.stack([I[:,:, :-2,:-2], I[:,:,1:-1,1:-1]], axis = -1)

    batch = tf.cond(tf.shape(batch)[-1] > 1,
        lambda: tf.image.rgb_to_grayscale(batch), lambda: batch)

    Ix = batch[:, 1:, 1:, 1:, :] - batch[:, 1:  , 1:  ,  :-1, :]
    Iy = batch[:, 1:, 1:, 1:, :] - batch[:, 1:  ,  :-1, 1:  , :]
    It = batch[:, 1:, 1:, 1:, :] - batch[:,  :-1, 1:  , 1:  , :]

    Ixx = tf.nn.conv3d(Ix * Ix, tf.ones([1,fs,fs,1,1]), [1,1,1,1,1], 'SAME')
    Iyy = tf.nn.conv3d(Iy * Iy, tf.ones([1,fs,fs,1,1]), [1,1,1,1,1], 'SAME')
    Ixy = tf.nn.conv3d(Ix * Iy, tf.ones([1,fs,fs,1,1]), [1,1,1,1,1], 'SAME')
    Ixt = tf.nn.conv3d(It * Ix, tf.ones([1,fs,fs,1,1]), [1,1,1,1,1], 'SAME')
    Iyt = tf.nn.conv3d(It * Iy, tf.ones([1,fs,fs,1,1]), [1,1,1,1,1], 'SAME')

    """

    | Vx |    | Ixx  Ixy |-1  | -Ixt |
    |    | =  |          |    |      |
    | Vy |    | Ixy  Iyy |    | -Iyt |

                1   |  Iyy  -Ixy |  | -Ixt |
           =  ----- |            |  |      |
               det  | -Ixy   Ixx |  | -Iyt |

                1   | IxyIyt-IyyIxt |
           =  ----- |               |
               inv  | IxyIxt-IxxIyt |

    det = IxxIyy-Ixy^2

    """

    Vx = Ixy * Iyt - Iyy * Ixt
    Vy = Ixy * Ixt - Ixx * Iyt
    det = Ixx * Iyy - Ixy * Ixy

    Vx = tf.where(tf.less(tf.abs(det), 1e-5), tf.zeros_like(Vx), Vx / det)
    Vy = tf.where(tf.less(tf.abs(det), 1e-5), tf.zeros_like(Vy), Vy / det)

    V = tf.concat([Vx, Vy], axis = -1)
    V = tf.clip_by_value(V, -10, 10)

    return V

def optical_flow_lk(batch, fs, pl = 1):

  if pl > 1:
    i_batch, _ = batch2vid(batch)

  flow = None
  for i in range(pl):

      if i == 0:
          flow = lk(batch, fs)
          h_flw_o = tf.shape(flow)[2]
          w_flw_o = tf.shape(flow)[3]
          continue

      r_batch = tf.image.resize_bilinear(i_batch, [h // (i+1), w // (i+1)])
      r_batch = tf.reshape(r_batch, [n, f, h // (i+1), w // (i+1), c])

      r_flow = lk(r_batch, fs)
      f_flw = tf.shape(r_flow)[1]
      h_flw = tf.shape(r_flow)[2]
      w_flw = tf.shape(r_flow)[3]
      r_flow = tf.reshape(r_flow, [n * f_flw, h_flw, w_flw, 2])
      r_flow = tf.image.resize_bilinear(r_flow, [h_flw_o, w_flw_o])
      r_flow = tf.reshape(r_flow, [n, f_flw, h_flw_o, w_flw_o, 2])

      flow = tf.concat((flow, r_flow * (i + 1)), axis = -1)

  return flow

def OpticalFlowLK(fs):
  def flow_l(x):
      x_flow = optical_flow_lk(x, fs, 1)
      return x_flow
  return Lambda(flow_l)

def concat_chunks(x, length):

    num_chunks, chunk_size, _, _, _ = x.shape
    x = tf.split(x, num_chunks, axis=0)
    x = [ v[0] for v in x ]

    if length % chunk_size == 0 or length < chunk_size:
      return tf.concat(x, axis=0)[:length]

    remainder = x[-1]
    x = tf.concat(x[:-1], axis=0)
    zr = (0, 0)
    x = tf.pad(x, [(0, length - x.shape[0])] + [zr] * 3)

    return tf.concat([x[:-chunk_size], remainder], axis=0)


def DSvid(c):
  return Lambda(lambda x : x[..., c:-c, c:-c, :])

class ChunkPosterior():

    def __init__(self, family):
        self.family = family

    def d(self, params):
        if self.family == 'Normal':
            return tfpd.Normal(params, tf.constant(0.01))
        elif self.family == 'Bernoulli':
            return tfpd.Bernoulli(logits = params)

    def log_prob(self, x, params):
        return self.d(params).log_prob(x)


