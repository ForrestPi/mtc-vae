import yaml
import tensorflow as tf

from os import path, makedirs
from argparse import ArgumentParser

def get_args(requires_checkpoint=False):

    parser = ArgumentParser(
        description='Tensorflow implementation of MTC-VAE')

    parser.add_argument('--config', type=str,
        help='Path to configuration file')
    
    parser.add_argument('--logdir', type=str,
        help='Where to store the results')
    
    parser.add_argument('--mgrowth', action='store_true',
        help='Whether to use GPU memory growth.')

    args = vars(parser.parse_args())

    local_config = path.join(args['logdir'], 'config.yaml')
    if args['config'] is None:
        args['config'] = local_config

    yargs = yaml.load(open(args['config'], 'r'), Loader=yaml.FullLoader)

    makedirs(args['logdir'], exist_ok=True)
    with open(local_config, 'w') as conf_file:
        yaml.dump(yargs, conf_file)

    args.update(yargs)

    checkpoint_path = path.join(args['logdir'], 'checkpoints', 'checkpoint')
    if requires_checkpoint and not path.exists(checkpoint_path):
        raise Exception('Checkpoint not found')

    if args['mgrowth']:
        for device in tf.config.experimental.list_physical_devices('GPU'):
            tf.config.experimental.set_memory_growth(device, True)

    return args
