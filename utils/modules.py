import numpy as np
from tensorflow.keras import Model
from utils import model_utils as U
from tensorflow.keras import layers as L

def encoder(in_sh, lat_sh, cf, suffix, fs=4, strd=2, lk=False):

    x_in = L.Input(shape = in_sh)

    # Dynamic filter size function
    gfs = lambda s : [min(fs, -(in_sh[0] // -s)), fs, fs]

    cnv = U.OpticalFlowLK(fs)(x_in) if lk else x_in

    for i in range(4):
        name = 'cnv' + suffix + str(i + 1)
        cnv = U.sConv3d(cf * 2**i, gfs(2**i), strd, name)(cnv)
        cnv = L.ReLU()(L.BatchNormalization()(cnv))

    cnv = L.Flatten()(cnv)

    mu = U.Dense(lat_sh, 'mu' + suffix)(cnv)
    sg = U.Dense(lat_sh, 'sg' + suffix)(cnv)
    sg = U.Softplus()(sg)

    return Model(inputs = x_in, outputs = [mu, sg], name = 'enc' + suffix)


def decoder(z_sh, w_sh, out_sh, channel_factor, chunk_posterior, fs=4, strd=2):

    zAin = L.Input(shape = z_sh)
    zMin = L.Input(shape = w_sh)

    cf = channel_factor
    chunk_size, height, width, n_channels = out_sh

    # Filter size functions
    zsz = lambda n : -(n // -16)
    z_sh = [1, zsz(height), zsz(width), 8 * cf]
    f0, f1, ffs = U.dyn_fs(chunk_size)

    z_in  = L.Concatenate()([zAin, zMin])
    dns   = U.Dense(np.prod(z_sh), 'dnsD')(z_in)
    dcnv5 = U.Reshape(z_sh)(dns)
    dcnv5 = L.ReLU()(L.BatchNormalization()(dcnv5))

    dcnv4 = U.vDeconv3d(cf * 4, [f0, fs, fs], strd, 'dcnv4')(dcnv5)
    dcnv4 = L.ReLU()(L.BatchNormalization()(dcnv4))
    
    dcnv3 = U.sDeconv3d(cf * 2, fs, 2, 'dcnv3')(dcnv4)
    dcnv3 = U.DSvid(2)(dcnv3)
    dcnv3 = L.ReLU()(L.BatchNormalization()(dcnv3))

    dcnv2 = U.vDeconv3d(cf, [f1, fs, fs], strd, 'dcnv2')(dcnv3)
    dcnv2 = L.ReLU()(L.BatchNormalization()(dcnv2))
    
    dcnv1 = U.sDeconv3d(n_channels, fs, 2, 'dcnv1')(dcnv2)
    dcnv1 = U.DSvid(2)(dcnv1)

    if ffs > 1:
        dcnv1 = U.vConv3d(n_channels, [ffs, 1, 1], 1, 'adj1')(dcnv1)

    x_out = U.Sigmoid()(dcnv1)
    x_par = U.Sigmoid()(dcnv1) if chunk_posterior == 'Normal' else dcnv1

    return Model(inputs=[zAin, zMin], outputs=[x_par, x_out], name='dec')
