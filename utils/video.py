import os
import numpy as np
from utils import model_utils as U
from PIL import Image, ImageDraw, ImageFont

def mosaic_dt_videos(vid_list, pad = 5):

    zr = (0, 0)
    pd = (pad, pad)
    vid_list = np.pad(vid_list, [zr, zr, zr, pd, pd, zr])

    H, W, f, h, w, c = vid_list.shape

    vid_list = np.transpose(vid_list, [2, 0, 3, 1, 4, 5])
    vid_list = np.reshape(vid_list, [f, H * h, W * w, c])

    return vid_list

def cr_video(orig, rct, lens, pad = 5):

    def pad_vid(x, l, lm):
        return np.pad(x[:l], [(0, lm - l)] + [(0,0)] * 3, mode = 'edge')

    n = len(orig)
    lm = np.max(lens)
    _, h, w, c = orig[0].shape

    for i in range(n):
        orig[i] = pad_vid(orig[i], lens[i], lm)
        for j in range(n):
            rct[i][j] = pad_vid(rct[i][j], lens[j], lm)

    rct = mosaic_dt_videos(np.array(rct), pad = pad)
    row = mosaic_dt_videos(np.array([orig]), pad = pad)
    col = np.reshape(row, [lm, h + 2 * pad, -1, w + 2 * pad, c])
    col = np.transpose(col, (0, 2, 1, 3, 4))
    col = np.reshape(col, [lm, -1, w + 2 * pad, c])

    txt = Image.new(mode = 'P', size = (w + 2 * pad + 2, h + 2 * pad + 1))
    draw = ImageDraw.Draw(txt)
    fnt = os.path.join(os.path.dirname(__file__), 'arial.ttf')
    fnt = ImageFont.truetype(font = fnt, size = 20)
    draw.text((8,  8), 'Driving', font = fnt, fill = 255)
    draw.text((5, 45), 'Source',  font = fnt, fill = 255)

    txt = np.float32(np.array(txt)) / 255.0
    mh = txt.shape[1] // 2
    txt[:mh ,  0] = 1 
    txt[ mh:, -1] = 1
    txt[ mh , : ] = 1

    zr = (0, 0)
    txt = np.tile(txt[None,...,None], [lm, 1, 1, c])
    row = np.pad(row, [zr, (0, 1),   zr,   zr])
    col = np.pad(col, [zr,   zr,   (1, 1), zr])

    row[:, -1, ...] = 1
    col[..., -1, :] = 1

    rct = np.concatenate([row, rct], axis = 1)
    col = np.concatenate([txt, col], axis = 1)

    return np.concatenate([col, rct], axis = 2)

def get_vid(x, model):

    x, length, _ = x.next()
    n = len(x)

    mosaic = [ [None for j in range(n)] for i in range(n) ]

    for i in range(n):
        for j in range(n):
            if i == j:
                mosaic[i][j] = model.reconstruct(x[i], length[i])
            elif i < j:
                mosaic[i][j], mosaic[j][i] = model.cross_reenact(
                    x[i], x[j], length[i], length[j])

    x_whole = [ U.concat_chunks(x[i], length[i]) for i in range(n) ]

    return cr_video(x_whole, mosaic, length, pad = 5)
