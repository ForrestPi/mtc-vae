import numpy as np
import tensorflow as tf

def text_summary(args, writer):

    argsd = vars(args)
    argsd['frame_size'] = str(argsd['frame_size'])

    with writer.as_default():
        tf.summary.text('config', np.array(list(argsd.items()),
                                                    dtype = np.str), step = 0)
