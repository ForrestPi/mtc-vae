import imageio
import numpy as np
from PIL import Image
import tensorflow as tf
from os import path, makedirs, system

def write_video_gif(file_name, vid):

    vid = vid if isinstance(vid, np.ndarray) else vid.numpy()
    makedirs(path.dirname(file_name), exist_ok=True)
    if(vid.shape[-1] == 1):
        vid = np.repeat(vid, 3, axis=-1)

    vid = (vid * 255).astype(np.uint8)

    imageio.mimsave('%s.gif' % (file_name), vid)

def file2items(pth):

    with open(pth, 'r') as f:
        f_lines = f.read().splitlines()
    itm = []
    for f_line in f_lines:
        itm.append(f_line.split(' '))

    return np.array(itm)

def save_mosaic_image(file_name, vid):

    vid = (vid.numpy() * 255).astype(np.uint8)

    d, h, w, c = vid.shape
    H = int(np.sqrt(d))
    W = int(np.ceil(d / H))

    zr = (0, 0)
    vid = np.pad(vid, [(0, H * W - d), zr, zr, zr])

    vid = np.reshape(vid, [H, W, h, w, c])
    vid = np.transpose(vid, [0, 2, 1, 3, 4])
    vid = np.reshape(vid, [H * h, W * w, c])

    imageio.imsave(file_name, vid)


'''
Reads a video from disk and pre-processes it depending on the requirement from
the model
'''
def read_video(filename, length, chunk_shape, train, shuffle_frames=False):

    chunk_size, height, width, n_channels = chunk_shape

    filename = filename if isinstance(filename, np.str_) \
        else filename.numpy().decode()
    vid = Image.open(filename)
    length = int(length) if isinstance(length, np.str_) else int(length.numpy())

    # Trnsform image mosaic into video

    H = int(np.sqrt(length))
    W = int(np.ceil(length / H))

    vid = vid.resize((width * W, height * H))

    vid = vid.convert('RGB') if n_channels == 3 else vid.convert('L')
    vid = tf.keras.preprocessing.image.img_to_array(vid) / 255.

    vid = np.stack(np.split(vid, H, axis=0), axis=0)
    vid = np.stack(np.split(vid, W, axis=2), axis=1)
    vid = np.reshape(vid, [H * W, height, width, -1])[:length]

    # If training, randomly sample one chunk and randomly flip it
    if train:

        if shuffle_frames:

            vid = vid[np.random.choice(length, chunk_size,
                replace=chunk_size>length)]

            return vid, length

        vid = vid if chunk_size == length else np.pad(vid,
            [(0, chunk_size - length % chunk_size)] + [(0, 0)]*3, mode='edge')

        if length > chunk_size:
            r = np.random.randint(length - chunk_size + 1) 
            vid = vid[r : r + chunk_size,...]

        if np.random.randint(2):
            vid = np.flip(vid, 2)

        return vid, length

    '''
    If it is not training, return the whole video
    If chunk size does not divide total length, get the remainder chunk from
    the end of the video
    '''
    remainder = vid[-chunk_size:] \
                    if length > chunk_size and length % chunk_size else None

    # Pad video to fit chunk division
    vid = np.pad(vid, [(0, chunk_size - length % chunk_size)] + [(0, 0)] * 3,
        mode = 'edge')
    vid = np.split(vid, vid.shape[0] // chunk_size, axis=0)

    '''
    Replace last chunk for the remainder when chunk size does not divide total
    length
    '''
    if remainder is not None:
        vid[-1] = remainder

    return np.stack(vid), length
