import numpy as np
from utils import io
import tensorflow as tf

class Dataset():

    def __init__(self, chunk_shape, order, batch_size, items,
        appearance_indices, train, paired, shuffle, repeat):

        self.chunk_shape = chunk_shape.copy()
        self.chunk_shape[0] = chunk_shape[0] * order if train \
            else chunk_shape[0]

        self.order = order
        self.train = train
        self.batch_size = batch_size

        items = io.file2items(items) if isinstance(items, str) \
            else items
        size = items.shape[0]

        path, length, factors = (items[:,0], items[:,1], items[:,2:])
        matches = [[]] * size

        if paired and train:

            app_indices = {}
            for i in range(size):
                appearance = '_'.join(factors[i, appearance_indices])
                if appearance not in app_indices.keys():
                    app_indices[appearance] = []
                app_indices[appearance].append(i)

            for k in app_indices.keys():
                indices = app_indices[k]
                for i in indices:
                    matches[int(i)] = '_'.join([str(s) for s in indices])

            matches = np.array(matches)
        else:
            matches = np.array([str(i) for i in range(size)])

        ds = tf.data.Dataset.from_tensor_slices((path,length,factors,matches))
        ds = ds.shuffle(size) if shuffle else ds
        ds = ds.repeat() if repeat else ds

        if paired:
            self.all_paths = path
            self.all_lengths = length

        out_list = [tf.float32] * 2 + [tf.int64, tf.string]
        ds = ds.map(lambda path, length, factors, matches : tuple(
            tf.py_function(self.get_item, [path, length, factors, matches],
                out_list)), num_parallel_calls=tf.data.experimental.AUTOTUNE)

        pad_str = tuple([[None] * 5] * 2 + [[None]] * 2)
        ds = ds.batch(batch_size) if train else ds.padded_batch(
            batch_size, padded_shapes = pad_str)
        ds = ds.prefetch(buffer_size = 2)

        self.iterator = iter(ds)
        self.size = size
        self.factors = factors

    def read_video(self, path, length, shuffle_frames=False):
        return io.read_video(path, length, self.chunk_shape, self.train,
            shuffle_frames)

    def get_item(self, path, length, factors, matches):

        im, lng = self.read_video(path, length)

        matches = str(matches.numpy().decode()).split('_')

        if len(matches) <= 1:
            return im, im, np.array([lng]), factors

        i = int(np.random.choice(matches))
        im_app, _ = self.read_video(self.all_paths[i], self.all_lengths[i],
            shuffle_frames=True)

        return im, im_app, np.array([lng]), factors

    def next(self):
        return next(self.iterator)


class TrainingDatasets():

    def __init__(self, chunk_shape, order, batch_size, train_list, test_list,
        paired, appearance_indices, prompt_batch_size, **kwargs):

        # Chunk datasets for training and per-epoch metric calculation

        self.chunk_train_ds = Dataset(chunk_shape, order, batch_size,
            train_list, appearance_indices, train=True, paired=paired,
            shuffle=True, repeat=True)

        self.chunk_test_ds = Dataset(chunk_shape, order, batch_size,
            test_list, appearance_indices, train=True, paired=False,
            shuffle=True, repeat=True)

        # Full-lenght video datasets for reenactment examples

        self.full_train_ds = FullChunkedDataset(chunk_shape, prompt_batch_size,
            train_list, appearance_indices, repeat=True)

        self.full_test_ds = FullChunkedDataset(chunk_shape, prompt_batch_size,
            test_list, appearance_indices, repeat=True)


class FullChunkedDataset(Dataset):

    def __init__(self, chunk_shape, embed_batch_size, data_list,
        appearance_indices, repeat=False, **kwargs):

        Dataset.__init__(self, chunk_shape, None, embed_batch_size, data_list,
            appearance_indices, train=False, paired=False, shuffle=False,
            repeat=repeat)

    def next(self):

        x, _, length, factors = next(self.iterator)
        n = x.shape[0]

        length = length[:, 0]
    
        num_chunks = np.ceil(length / x.shape[2]).astype(np.int32)

        x_chunk = [x[i, :num_chunks[i]] for i in range(n)]

        tiled_factors = tf.concat([ tf.tile(factors[None, i],
            [num_chunks[i], 1]) for i in range(n) ], axis=0)

        return x_chunk, length, tiled_factors


class ReenactChunkDataset():

    def __init__(self, chunk_shape, reenact_batch_size, pairs_list,
        paired, appearance_indices, prompt_batch_size, **kwargs):


        items = io.file2items(pairs_list)
        items_source, items_driving = np.split(items, 2, axis = 1)

        self.source = FullChunkedDataset(chunk_shape, reenact_batch_size,
            items_source, appearance_indices)

        self.driving = FullChunkedDataset(chunk_shape, reenact_batch_size,
            items_driving, appearance_indices)

        self.size = self.source.size

    def next(self):

        x_source, length_source, _ = self.source.next()
        x_driving, length_driving, _ = self.driving.next()

        return x_source, length_source, x_driving, length_driving
