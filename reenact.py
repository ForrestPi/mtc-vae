import numpy as np
from tqdm import tqdm
import tensorflow as tf
from utils import config, io
from os import path, makedirs
from models.mtc_vae import MTC_VAE
from datasets import ReenactChunkDataset

if __name__ == '__main__':

    args = config.get_args(requires_checkpoint=True)

    data = ReenactChunkDataset(**args)
    model = MTC_VAE(None, **args)

    model.train = False
    out_path = path.join(args['logdir'], 'reenact')
    makedirs(out_path, exist_ok=True)

    pbar = tqdm(total=data.size, desc='Performing reenactment',
        bar_format='{l_bar}{bar}| {elapsed}<{remaining}>')
    j = 0
    while(True):
        try:

            x_source, _, x_driving, length = data.next()

            for i in range(len(x_source)):
                x_reenact = model.reenact(x_source[i], x_driving[i], length[i])
                depth, height, width, n_channels = x_reenact.shape

                name = '%07d.png' % (j)
                io.save_mosaic_image(path.join(out_path, name), x_reenact)
                
                j += 1
                pbar.update(1)

        except StopIteration:
            break

    pbar.close()
    print('Videos saved in ' + out_path)

                      
