FROM tensorflow/tensorflow:2.3.0-gpu

MAINTAINER mipl

# Install additional python dependencies

COPY ./ /home/

RUN apt-get -y update -qq && \
    apt-get install ffmpeg imagemagick -y \
    &&\
    # Clean the install from sources
    apt-get autoclean autoremove &&\
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*

RUN pip install --no-cache-dir --upgrade pip; \
    pip install --no-cache-dir -r /home/requirements.txt

# Tensorboard
EXPOSE 6006

WORKDIR /home
