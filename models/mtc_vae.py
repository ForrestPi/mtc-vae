import numpy as np
import tensorflow as tf

from utils import model_utils as U
from models.base_model import VideoModel
from utils.modules import encoder, decoder
from tensorflow_probability import distributions as tfd

class MTC_VAE(VideoModel):

    def __init__(self, data, chunk_shape, logdir, batch_size, dim_z, dim_w,
        checkpoints_to_keep, chunk_posterior, channel_factor, order,
        loss_weights, learning_rate, **kwargs):

        self.chunk_posterior = chunk_posterior
        self.channel_factor = channel_factor
        self.learning_rate = learning_rate
        self.chunk_size = chunk_shape[0]
        self.dim_z = dim_z
        self.dim_w = dim_w
        self.order = order

        self.beta    = loss_weights['beta']
        self.vlambda = loss_weights['vlambda']

        self.cp = U.ChunkPosterior(chunk_posterior)

        # Logger configuration

        self.n_losses = ['elbo', 'logp', 'brl', 'kl_a', 'kl_m']
        self.f_losses = ['total', 'inp', 'inp',  'lat',   'lat']

        self.n_hist = [ 'zA',  'zM', 'x_rec', 'x_rtt']
        self.f_hist = ['lat', 'lat', 'inp',   'inp']

        self.i_losses = [0, 1, 2, 3, 4]
        self.i_hist = []

        VideoModel.__init__(self, data, chunk_shape, logdir, batch_size,
            checkpoints_to_keep)

    def build_model(self):

        x_sh = (self.chunk_size, self.height, self.width, self.n_channels)
        z_sh = (self.dim_z, )
        w_sh = (self.dim_w, )

        # Streams

        channel_factor = self.channel_factor
        self.encA = encoder(x_sh, self.dim_z, channel_factor, 'A')
        self.encM = encoder(x_sh, self.dim_w, channel_factor, 'M', lk=True)

        self.dec  = decoder(z_sh, w_sh, x_sh, channel_factor * 2,
            self.chunk_posterior, strd=[1, 2, 2])

        self.modules = [self.encA, self.encM, self.dec]

        # VAE priors

        self.pAz = tfd.Normal(tf.zeros(self.dim_z), tf.ones(self.dim_z))
        self.pMz = tfd.Normal(tf.zeros(self.dim_w),  tf.ones(self.dim_w))

        # Trainable variables

        self.vae_vars = self.encA.trainable_variables \
                            + self.encM.trainable_variables \
                            + self.dec.trainable_variables

        # Optimizers

        self.vae_opt = tf.keras.optimizers.Adam(
            learning_rate=self.learning_rate)

        # Chckpoints

        self.ckpt = tf.train.Checkpoint(epoch=self.epoch,
                                         encA=self.encA,
                                         encM=self.encM,
                                          dec=self.dec,
                                      vae_opt=self.vae_opt)

    def forward(self, x_mot, x_app):

        out = self.forward_order_1(x_mot, x_app) if self.order == 1 \
              else self.forward_order_n(x_mot, x_app)
        out['elbo'] = -out['logp'] + self.vlambda * out['brl'] \
                            + self.beta * ( out['kl_a'] + out['kl_m'] )
        return out


    def forward_order_n(self, x_mot, x_app):

        order = self.order

        # Each element of the batch are O consecutive chunks along axis 1
        xO_mot = tf.split(x_mot, order, axis = 1)
        xO_app = tf.split(x_app, order, axis = 1)

        '''
        The chunks are separated an concatenated along axis 0, and then
        treated as independent videos
        '''
        x_spl_mot = tf.concat(xO_mot, 0)
        x_spl_app = tf.concat(xO_app, 0)

        zA, zM, La, Lm = self.encode(x_spl_mot, x_spl_app)

        '''
        Generating all the possible combinations of apperance--motion
        representations (O^2 per video)
        '''
        zAd = tf.stack(tf.split(zA, order, axis = 0))
        zAd = tf.reshape(tf.tile(zAd, [1, order, 1]), [-1, self.dim_z])
        zMd = tf.tile(zM, [order, 1])

        # Reconstruction of the chunks and parameters of the chunk posteriors
        x_params, x_rec = self.dec([zAd, zMd])

        x_params = tf.stack(tf.split(x_params, order, axis=0))

        # x_params.shape = [O, O*batch_size,chunk_size,height,width,n_channels]
        # x_spl.shape    = [   O*batch_size,chunk_size,height,width,n_channels]

        '''
        The different O appearance representations are along the first axis
        of x_params, so each x_params[i] contains a sequence of O chunks that
        must be simmilar to the original batch
        '''

        # log-likelihood calculation
        rec_l_prob = self.cp.log_prob(x_spl_mot, x_params) / float(order)
        Lr = tf.reduce_sum(tf.reduce_mean(rec_l_prob, axis=1))

        '''
        The motion representations are reversed for reenactment, so the first
        video of the batch is reenacted w.r.t. the last, the second video,
        w.r.t. the second to last, and so on.
        Then, instead of generating the whole set of batch_size^2 possible
        reenactments, we generate only batch_size, to prevent memory overflow.
        Recall that the chunks are along the axis 0.
        '''
        zMr = tf.stack(tf.split(zM, order, 0))
        zMr = tf.reshape(tf.reverse(zMr, [1]), [-1, self.dim_w])
        zMrd = tf.tile(zMr, [order, 1])

        x_rtt_params, x_rtt = self.dec([zAd, zMrd])

        '''
        x_rtt_params.shape:
          from [      O^2 * batch_size, chunk_size, height, width, n_channels]
          to   [O, O,    batch_size,    chunk_size, height, width, n_channels]
        '''
        x_rtt_params = tf.stack(tf.split(x_rtt_params, order, axis=0))
        x_rtt_params = tf.stack(tf.split(x_rtt_params, order, axis=1), axis=0)

        '''
        Blind reenactment loss in vector form. (Recall that the complexity of
        calculating this loss is cubic w.r.t. O.)
        The second dimension of x_rtt_params is broadcasted O times along
        itself, to yield a one-vs-one comparison. This correspons to the O^2
        to attain the same reenacted video.
        '''
        kl_brl = tfd.kl_divergence(self.cp.d(x_rtt_params[:,None,...]),
                self.cp.d(x_rtt_params[:,:,None,...])) / order / (order - 1)
        # Average along the batch dimension
        Lb = tf.reduce_sum(tf.reduce_mean(kl_brl, axis=3))

        return { 'logp' : Lr, 'brl' : Lb, 'kl_a' : La, 'kl_m' : Lm,
            'x_rec' : x_rec, 'x_rtt' : x_rtt, 'zA' : zA, 'zM' : zM }

    def forward_order_1(self, x_mot, x_app):

        zA, zM, La, Lm = self.encode(x_mot, x_app)
        x_params, x_rec = self.dec([zA, zM])

        # Reconstruction loss
        rec_l_prob = self.cp.log_prob(x_mot, x_params)
        Lr = tf.reduce_sum(tf.reduce_mean(rec_l_prob, axis=0))

        zMr = tf.reverse(zM, [0])

        # Reenacted videos (no way to calculate the blind reenactment loss)
        x_rtt_params, x_rtt = self.dec([zA, zMr])
        Lb = 0

        return { 'logp' : Lr, 'brl' : Lb, 'kl_a' : La, 'kl_m' : Lm,
            'x_rec' : x_rec, 'x_rtt' : x_rtt, 'zA' : zA, 'zM' : zM }


    def embed(self, x):

        zA, _ = self.encA(x)
        zM, _ = self.encM(x)

        z = tf.concat([zA, zM], axis = -1)

        return z

    def reenact(self, src, drv, l):
        
        # Less general than cross_reenact(), but saves time and memory

        c = tf.shape(drv)[0].numpy()

        zA, _ = self.encA(src[None, 0])
        zM, _ = self.encM(drv)

        _, rtt = self.dec([tf.tile(zA[None, 0], [c, 1]), zM])

        return U.concat_chunks(rtt, l)


    def cross_reenact(self, x0, x1, l0, l1):

        c0 = tf.shape(x0)[0].numpy()
        c1 = tf.shape(x1)[0].numpy()

        src = tf.stack( [x0[0], x1[0]], axis=0)
        drv = tf.concat([x1,    x0   ], axis=0)

        zA, _ = self.encA(src)
        zM, _ = self.encM(drv)

        zA0 = tf.tile(zA[None, 0], [c1, 1])
        zA1 = tf.tile(zA[None, 1], [c0, 1])

        zAc = tf.concat([zA0, zA1], axis = 0)

        _, rec = self.dec([zAc, zM])

        x01, x10 = tf.split(rec, [c1, c0])

        x01 = U.concat_chunks(x01, l1)
        x10 = U.concat_chunks(x10, l0)

        return x01, x10

    def reconstruct(self, x, l):

        c = tf.shape(x)[0].numpy()

        zA, _ = self.encA(x[None, 0])
        zM, _ = self.encM(x)

        _, rec = self.dec([tf.tile(zA, [c, 1]), zM])

        return U.concat_chunks(rec, l)
        

    def encode(self, x_mot, x_app):

        muA, sgA = self.encA(x_app)
        muM, sgM = self.encM(x_mot)

        qAz_x, qMz_x = (tfd.Normal(muA, sgA), tfd.Normal(muM, sgM))

        zA = self.dsample(qAz_x)
        zM = self.dsample(qMz_x)

        kl_a = tfd.kl_divergence(qAz_x, self.pAz)
        kl_m = tfd.kl_divergence(qMz_x, self.pMz)

        lta_loss = tf.reduce_mean(tf.reduce_sum(kl_a, axis=-1))
        ltm_loss = tf.reduce_mean(tf.reduce_sum(kl_m, axis=-1))

        return zA, zM, lta_loss, ltm_loss

    @tf.function
    def gen_step(self):

        self.train = True
        x_mot, x_app, _, _ = self.data.chunk_train_ds.next()

        with tf.GradientTape() as vae_tape:
            out = self.forward(x_mot, x_app)

        vae_grads = vae_tape.gradient(out['elbo'], self.vae_vars)
        self.vae_opt.apply_gradients(zip(vae_grads, self.vae_vars))

        return out

    def dsample(self, p):
        # Robust sampling method
        return p.sample() if self.train else p.mode()
