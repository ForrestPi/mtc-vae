from os import path
from utils import io
import tensorflow as tf

class BaseModel:

    def __init__(self, data, logdir, batch_size, checkpoints_to_keep):

        self.logdir = logdir
        self.batch_size = batch_size
        self.train = True

        self.data = data

        self.epoch = tf.Variable(1, name='epoch', dtype=tf.int32,
            trainable=False)

        self.build_model()

        ckpt_path = path.join(self.logdir, 'checkpoints/')
        self.ckpt_manager = tf.train.CheckpointManager(self.ckpt, ckpt_path,
            max_to_keep=checkpoints_to_keep)

        self.ckpt.restore(self.ckpt_manager.latest_checkpoint)

    def build_model(self, input_data):
        raise NotImplementedError

class VideoModel(BaseModel):

    def __init__(self, data, chunk_shape, logdir, batch_size,
        checkpoints_to_keep):

        self.height, self.width, self.n_channels = chunk_shape[1:]

        BaseModel.__init__(self, data, logdir, batch_size, checkpoints_to_keep)
